#!/bin/bash

# Source: https://stackoverflow.com/questions/4414297/unix-bash-script-to-embolden-underline-italicize-specific-text/4414355#4414355
underline=$(tput smul)
nounderline=$(tput rmul)
bold=$(tput bold)
normal=$(tput sgr0)
