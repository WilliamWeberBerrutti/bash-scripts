#!/bin/bash

git_preparation_usage ()
{
cat << EOF

${bold}USAGE${normal}

    git_preparation -o <${underline}ORIGIN LINK${nounderline}> -r <${underline}REMOTE LINK${nounderline}>

${bold}DESCRIPTION${normal}

    Prepares the local repo directory with ${underline}ORIGIN LINK${nounderline}, clones, initializes submodules, adds ${underline}REMOTE LINK${nounderline} as upstream, and prints the repo status after all operations are completed.

${bold}OPTIONS${normal}

    ${bold}-h, -help, --help${normal}
        Display help

    ${bold}-o , -origin, --origin${normal} ${underline}ORIGIN LINK${nounderline}
        Obtain the origin link

    ${bold}-r , -remote, --remote${normal} ${underline}REMOTE LINK${nounderline}
        Obtain the remote link

EOF
}

merge_csv_files_usage ()
{
cat << EOF

${bold}USAGE${normal}

    merge_csv_files -i <${underline}INPUT FILES${nounderline}> -o <${underline}OUTPUT FILE${nounderline}>

${bold}DESCRIPTION${normal}

    Merges all CSV ${underline}INPUT FILES${nounderline} into ${underline}OUTPUT FILE${nounderline}. The ${underline}INPUT FILES${nounderline} must have the same prefix on their names.

${bold}OPTIONS${normal}

    ${bold}-h, -help, --help${normal}
        Display help

    ${bold}-i , -input, --input${normal} ${underline}INPUT FILES${nounderline}
        Obtain the input files

    ${bold}-o , -output, --output${normal} ${underline}OUTPUT FILE${nounderline}
        Obtain the output file

EOF
}
