#!/bin/bash

# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# https://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="${bold}|${normal} %F ${bold}|${normal} %I:%M:%S %p ${bold}|${normal} "
export HISTCONTROL=ignoreboth:erasedups
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# https://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_history

# Force prompt to write history after every command.
# https://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
shopt -s histappend
stophistory ()
{
    PROMPT_COMMAND="bash_prompt_command"
    echo 'History recording stopped. Make sure to `kill -9 $$` at the end of the session.'
}
