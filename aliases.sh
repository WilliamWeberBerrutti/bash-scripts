#!/bin/bash

alias AdGuardHome="/opt/AdGuardHome/AdGuardHome"
alias cls="clear"
alias cns="check_network_socket"
alias disabletrim="sudo systemctl disable fstrim.timer"    
alias editsddm="sudo nano /etc/sddm.conf.d/kde_settings.conf"
alias enabletrim="sudo systemctl enable fstrim.timer"
alias fixaoe2desync="fix_aoe2de_desync"
alias gitprep="git_preparation"
alias ls_aur="ls /var/tmp/pamac-build-$USER/" 
alias ls_listen="list_listening_procs"
alias mergecsv="merge_csv_files"
alias more="less"
alias open_log_file="kate /var/log/pacman.log"
alias os_install_time="os_installation_time"
alias pacmanrud="sudo pacman -Qdtq | sudo pacman -Rs -"   # rud = remove unnecessary dependencies    # Source: https://wiki.archlinux.org/title/Pacman/Rosetta
alias pod="popd"
alias prepvm="prepare_virt_manager"
alias pud="pushd"
alias rebuild_module_deps_db="depmod"
alias regengrub="grub-mkconfig -o /boot/grub/grub.cfg" # Source: https://www.gnu.org/software/grub/manual/grub/html_node/Invoking-grub_002dmkconfig.html, https://wiki.archlinux.org/title/GRUB
alias show_cpu_virtualization_support="cat /proc/cpuinfo | grep -e 'svm' -e 'vmx'"
alias show_detailed_memory="cat /proc/meminfo"
alias show_device_nodes="ls /dev"
alias show_extra_configs="ls /etc/default"
alias show_gl_version="glxinfo | grep 'OpenGL version'" # Source: https://askubuntu.com/questions/47062/terminal-command-to-show-opengl-version
alias show_hostname_info="hostnamectl"
alias show_kde_vars="printenv | grep 'KDE'"
alias show_kernel_booted_command_line="cat /proc/cmdline"
alias show_kernel_parameters="sysctl -a"
alias show_loaded_modules="lsmod"
alias show_lvm_utilities="ls -lF /sbin/lv*"
alias show_open_files="show_open_files_in_dir"
alias show_qemu_supported_formats="qemu-img --help | grep 'Supported formats:'"
alias show_supported_filesystems="cat /proc/filesystems"
alias show_swap_space="cat /proc/swaps"
alias show_sysctl_rules="cat /etc/sysctl.d/*"
alias show_sys_details="sudo inxi -Fxxxza"
alias show_tempfs_usage="df -h /dev/shm"
alias show_virtual_memory_tuning_parameters="ls /proc/sys/vm"
alias sudo="sudo "
alias ubash=". ~/.bashrc; . ~/.bash_profile"
alias vulkan_driver_name="vulkaninfo | grep 'driverName' -m 1"
