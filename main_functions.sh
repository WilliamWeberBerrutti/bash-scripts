#!/bin/bash

# Source: https://unix.stackexchange.com/questions/20396/make-cd-automatically-ls#20398
cdls () { cd "$@" && ls -F; }

# $1 -> program name
check_network_socket ()
{
    sudo ss -tunelp | grep -i "$1"
}

# Source: https://adamtheautomator.com/convert-markdown-to-html/#Pandoc
convert_md_to_html ()
{
    if [[ ! -d converted ]] ; then
        mkdir converted
    fi

    for file in *
    do
        if [[ -f "$file" ]] ; then
            filename="$(basename -s .md $file)"
            pandoc "$file" -t html -o converted/"$filename".html
        fi
    done
}

# Should be executed after updates just in case.
# Source: https://www.protondb.com/app/813780#D0_SuRMYn8
fix_aoe2de_desync ()
{
    sudo pacman -Sy cabextract
    
    cp -v ~/.steam/steam/steamapps/common/AoE2DE/vc_redist.x64.exe ~/.steam/steam/steamapps/compatdata/813780/pfx/drive_c/windows/system32/vc_redist.x64.exe

    cd ~/.steam/steam/steamapps/compatdata/813780/pfx/drive_c/windows/system32/

    sudo cabextract vc_redist.x64.exe

    sudo cabextract a10

    sudo chown -R $USER:$USER .
}

git_preparation ()
{
    local options=$(getopt -l "help,origin:,remote:" -o ":ho:r:" -a -- "$@")

    if [[ $? -ne 0 ]] ; then
        printf "\nIncorrect options provided!\n"
        git_preparation_usage
        return
    fi

    eval set -- "$options"

    while true
    do
        case $1 in
        -h|--help)
            git_preparation_usage
            return
            ;;
        -o|--origin)
            shift
            local origin=$1
            ;;
        -r|--remote)
            shift
            local remote=$1
            ;;
        --)
            shift
            break
            ;;
        esac

    shift
    done

    if [[ -z $origin || -z $remote ]] ; then
        printf "\nIncorrect options provided!\n"
        git_preparation_usage
        return
    fi

    local reponame="$(basename $origin .git)"

    printf "\n"
    git clone $origin
    cd "$reponame"
    printf "\nWorking Directory: $PWD\n"
    printf "\nInitializing submodules...\n\n"
    git submodule update --init
    printf "Done!\n"
    printf "\nAdding upstream remote... "
    git remote add upstream $remote
    printf "Done!\n"
    printf "\nPrinting remotes:\n\n"
    git remote -v
    printf "\nPrinting git status:\n\n"
    git status
}

# Sources: https://computingforgeeks.com/install-and-manage-flatpak-applications-on-linux/, https://stackoverflow.com/questions/226703/how-do-i-prompt-for-yes-no-cancel-input-in-a-linux-shell-script/226724#226724
install_arch_flatpak ()
{
    sudo pacman -S flatpak
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    printf "The system needs to reboot to finish Flatpak installation. Continue?\n\n"

    select yn in "Yes" "No"; do
        case $yn in
            Yes) sudo reboot;;
            No) exit;;
        esac
    done
}

# Source: https://linuxize.com/post/check-listening-ports-linux/
list_listening_procs ()
{
    sudo lsof -nP -iTCP -sTCP:LISTEN +c 15
}

# Source: https://www.oodlestechnologies.com/blogs/How-to-convert-multiple-CSV-file-into-one-file-in-UBUNTU/
merge_csv_files ()
{
    local options=$(getopt -l "help,input:,output:" -o ":hi:o:" -a -- "$@")

    if [[ $? -ne 0 ]] ; then
        printf "\nIncorrect options provided!\n"
        merge_csv_files_usage
        return
    fi

    eval set -- "$options"
    while true
    do
        case $1 in
        -h|--help)
            merge_csv_files_usage
            return
            ;;
        -i|--input)
            shift
            local input_files=$1
            ;;
        -o|--output)
            shift
            local output_file=$1
            ;;
        --)
            shift
            break
            ;;
        esac

    shift
    done

    if [[ -z $input_files || -z $output_file ]] ; then
        printf "\nIncorrect options provided!\n"
        merge_csv_files_usage
        return
    fi

    printf "Merging CSV files... "
    awk 'FNR==1 && NR!=1{next;}{print}' "$input_files"* > "$output_file".csv
    printf "Done!\n"
}

# Source (for 'cut'): https://stackoverflow.com/questions/28185227/shell-script-substring-till-space/28185270#28185270
os_installation_time ()
{
    head -n1 /var/log/pacman.log | cut -d ' ' -f1
}

# Source (base): https://wiki.manjaro.org/index.php/Virt-manager
prepare_virt_manager ()
{
    printf "Enabling 'libvirtd.service'... "
    sudo systemctl enable libvirtd.service
    printf "Done!\n"

    printf "Starting 'libvirtd.service'... "
    sudo systemctl start libvirtd.service
    printf "Done!\n"

    printf "Setting user '$USER' to group 'libvirt'... "
    sudo usermod -a -G libvirt $USER
    printf "Done!\n"

    printf "Setting user '$USER' to group 'kvm... "
    sudo usermod -a -G kvm $USER
    printf "Done!\n"

    printf "\nPrintint status of 'libvirtd.service':\n\n"
    sudo systemctl status libvirtd.service
}

# Source (base): https://github.com/flatpak/flatpak/issues/709#issuecomment-565740284
set_flatpak_app_cursors ()
{
    for app_dir in $HOME/.var/app/*
    do
        app_name="$(basename $app_dir)"
        printf "Setting system cursor to the application '$app_name'... "
        flatpak --user override $app_name --filesystem=/$HOME/.icons/:ro
        printf "Done!\n"
    done
}

# $1 -> dir
show_open_files_in_dir ()
{
    lsof +D $1
}
