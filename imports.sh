#!/bin/bash

. "$BASH_SCRIPTS_DIR/styling.sh"
. "$BASH_SCRIPTS_DIR/histfile.sh"
. "$BASH_SCRIPTS_DIR/usage_functions.sh"
. "$BASH_SCRIPTS_DIR/main_functions.sh"
. "$BASH_SCRIPTS_DIR/aliases.sh"
