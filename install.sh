#!/bin/bash

sed -i -e '$a\\' ~/.bashrc &> /dev/null
printf "BASH_SCRIPTS_DIR=\"$(dirname "$(readlink -f imports.sh)")\"\n" >> ~/.bashrc
. ~/.bashrc
printf ". \"\$BASH_SCRIPTS_DIR/imports.sh\"\n" >> ~/.bashrc
exec bash