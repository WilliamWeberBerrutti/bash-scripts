# Bash Scripts

This repo has a collection of my personal Bash scripts, some were obtained from the internet, and some were developed by myself.

To install, simply execute "install.sh". It includes this repo's scripts into the user's .bashrc.

Feel free to execute, study, copy, modify, and share modifications freely!

Also feel free to give feedback!

Below is a brief description of each part:


## Aliases

**AdGuardHome:** shorthand for AdGuardHome's executable.

**cls**: calls "clear".

**cns**: calls "check_network_socket".

**disabletrim:** calls "disable_trim".

**editsddm:** edits KDE SDDM's settings.

**enabletrim:** calls "enable_trim".

**fixaoe2desync:** calls "fix_aoe2de_desync".

**gitprep:** calls "git_preparation".

**ls_aur:** lists all AUR packages currently installed.

**ls_listen:** calls "list_listening_processes".

**mergecsv:** calls "merge_csv_files".

**more:** replaces "more" command with "less", since the former is primitive.

**open_log_file:** open Pacman's log file.

**os_install_time:** calls "os_installation_time".

**pacmanrud**: removes unnecessary packages on an Arch system.

**pod**: calls "popd".

**prepvm**: calls "prepare_virt_manager".

**pud**: calls "pushd".

**rebuild_module_deps_db**: calls "depmod".

**regengrub**: regens GRUB config.

**show_cpu_virtualization_support**: shows if the current CPU has virtualization support (Intel or AMD compatible).

**show_detailed_memory**: shows detailed memory information.

**show_device_nodes**: shows all device nodes currently in use.

**show_extra_configs**: shows system's extra configs.

**show_gl_version:** prints OpenGL version of the host machine.

**show_hostname_info**: calls "hostnamectl".

**show_kde_vars:** prints KDE Plasma environment variables.

**show_kernel_booted_command_line**: shows which parameters the kernel was booted in the command line.

**show_kernel_parameters**: shows all kernel parameters and their values.

**show_loaded_modules**: calls "lsmod".

**show_lvm_utilities**: prints all LVM utilities currently installed in the system.

**show_open_files:** calls "show_open_files_in_dir".

**show_qemu_supported_formats**: prints all QEMU supported formats in the current computer.

**show_supported_filesystems**: prints all supported filesystems on the current computer.

**show_swap_space**: prints information about swap.

**show_sysctl_rules**: prints currently loaded sysctl rules.

**show_sys_details:** lists host system details.

**show_virtual_memory_tuning_parameters**: lists parameters related to memory tuning.

**sudo**: allows "sudo" to be used on aliases.

**ubash:** sources ".bashrc" and ".bash_profile".

**vulkan_driver_name:** lists the name of the Vulkan GPU driver.


## HistFile

Prepares the user's HISTFILE to have unlimited entries and a date time for every new entry.


## Main Functions

**cdls:** combines the execution of "cd" and "ls" commands.

**check_network_socket**: calls "ss" to investigate sockets.

**convert_md_to_html:** converts a list of Markdown files into a list of HTML files.

**disable_trim:** disables SSD trimming with fstrim.timer.

**enable_trim:** enables SSD trimming with fstrim.timer.

**fix_aoe2de_desync:** does a sequence of commands to prevent Age of Empires 2: Definitive Edition multiplayer desync when using Proton.

**git_preparation:** clones, initializes submodules, adds and prints remotes, and at the end prints status of the repository.

**install_arch_flatpak:** installs Flatpak in the Arch system.

**list_listening_procs**: lists all listening processes on the host machine.

**merge_csv_files:** merges multiple CSV files in one. It also keeps 1 header only.

**os_installation_time:** obtains the host operating system installation time.

**prepare_virt_manager:** enables libvirt.service and adds the current user to libvirt and kvm groups so it doesn't need root permissions to use VM's in virt-manager.

**set_flatpak_app_cursors:** sets the current system cursor to all Flatpak applications.

**show_open_files_in_dir**: lists all files which are opened in a dir.

**xrandrfix:** tries to fix XRandr display issues.


## Styling

Defines some variables for bold, italic, underline and normal texts.


## Usage Functions

Defines some help functions to aid on the execution of the main ones.


## Sources

https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash/52674277#52674277

https://stackoverflow.com/questions/255898/how-to-iterate-over-arguments-in-a-bash-script/255913#255913

https://wiki.archlinux.org/title/Pacman/Rosetta

https://askubuntu.com/questions/47062/terminal-command-to-show-opengl-version

https://stackoverflow.com/questions/4414297/unix-bash-script-to-embolden-underline-italicize-specific-text/4414355#4414355

https://unix.stackexchange.com/questions/20396/make-cd-automatically-ls#20398

https://adamtheautomator.com/convert-markdown-to-html/#Pandoc

https://www.protondb.com/app/813780#D0_SuRMYn8

https://computingforgeeks.com/install-and-manage-flatpak-applications-on-linux/

https://stackoverflow.com/questions/226703/how-do-i-prompt-for-yes-no-cancel-input-in-a-linux-shell-script/226724#226724

https://linuxize.com/post/check-listening-ports-linux/

https://www.oodlestechnologies.com/blogs/How-to-convert-multiple-CSV-file-into-one-file-in-UBUNTU/

https://stackoverflow.com/questions/28185227/shell-script-substring-till-space/28185270#28185270

https://wiki.manjaro.org/index.php/Virt-manager

https://github.com/flatpak/flatpak/issues/709#issuecomment-565740284

https://mywiki.wooledge.org/BashFAQ/035#Complex_nonstandard_add-on_utilities
